This page points to the PDF files hosted at [NXP site](http://nxp.com). It means the blog
does not keep a copy of the PDF files.

The table below shows the link to the file and the modification date for that file.

The modification date for the file is not exactly its **revision**, which is
related to the publication data. However the modification date is easily extracted
from PDF metadata.
