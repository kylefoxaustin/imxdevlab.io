This report was generated on Fri Aug 17 13:10:51 UTC 2018

|**Title**|**Last Modification**|
|[How to Reduce SoC Power when Running M4 with A53 on i.MX8M](https://www.nxp.com/docs/en/application-note/AN12225.pdf) |Aug 2018|
|[Software Solutions for Migration Guide from Aarch32 to Aarch64](https://www.nxp.com/docs/en/application-note/AN12212.pdf) |Aug 2018|
|[Tampering Application for i.MX7Dsabresd](https://www.nxp.com/docs/en/application-note/AN12210.pdf) |Jul 2018|
|[AN12185](https://www.nxp.com/docs/en/application-note/AN12185.pdf) |Jul 2018|
|[464410](https://www.nxp.com/docs/en/application-note/AN12134.pdf) |Jul 2018|
|[AN12135](https://www.nxp.com/docs/en/application-note/AN12135.pdf) |Jul 2018|
|[How to Enable Boot from QSPI Flash](https://www.nxp.com/docs/en/application-note/AN12108.pdf) |Jul 2018|
|[How to Enable Boot from Octal SPI Flash and SD Card](https://www.nxp.com/docs/en/application-note/AN12107.pdf) |Jul 2018|
|[Implement Low-Power Audio on i.MX8M](https://www.nxp.com/docs/en/application-note/AN12195.pdf) |Jun 2018|
|[i.MXRT1020 Product Lifetime Usage Estimates](https://www.nxp.com/docs/en/application-note/AN12203.pdf) |Jun 2018|
|[Power Consumption and Measurement of i.MX RT1020](https://www.nxp.com/docs/en/application-note/AN12204.pdf) |Jun 2018|
|[PMSM Field-Oriented Control on MIMXRT1050 EVK](https://www.nxp.com/docs/en/application-note/AN12169.pdf) |May 2018|
|[AN4581](https://www.nxp.com/docs/en/application-note/AN4581.pdf) |May 2018|
|[Freescale Semiconductor](https://www.nxp.com/docs/en/application-note/AN12188.pdf) |May 2018|
|[Freescale Semiconductor](https://www.nxp.com/docs/en/application-note/AN12189.pdf) |May 2018|
|[Freescale Semiconductor](https://www.nxp.com/docs/en/application-note/AN12187.pdf) |May 2018|
|[Measuring Interrupt Latency](https://www.nxp.com/docs/en/application-note/AN12078.pdf) |Apr 2018|
|[i.MXRT1050 Product Lifetime Usage Estimates](https://www.nxp.com/docs/en/application-note/AN12170.pdf) |Apr 2018|
|[AN12119](https://www.nxp.com/docs/en/application-note/AN12119.pdf) |Mar 2018|
|[i.MX 8M Dual / 8M QuadLite / 8M Quad Product Lifetime Usage](https://www.nxp.com/docs/en/application-note/AN12147.pdf) |Feb 2018|
|[AN12121](https://www.nxp.com/docs/en/application-note/AN12121.pdf) |Feb 2018|
|[AN Using the i.MXRT L1 Cache](https://www.nxp.com/docs/en/application-note/AN12042.pdf) |Jan 2018|
|[Developing a Camera Application with i.MX RT Series](https://www.nxp.com/docs/en/application-note/AN12110.pdf) |Dec 2017|
|[Developing a simple UVC device based on i.MX RT1050](https://www.nxp.com/docs/en/application-note/AN12103.pdf) |Dec 2017|
|[Power consumption measurement of i.MXRT1050](https://www.nxp.com/docs/en/application-note/AN12094.pdf) |Dec 2017|
|[Freescale Semiconductor](https://www.nxp.com/docs/en/application-note/AN12051.pdf) |Nov 2017|
|[Using Multi-Channel Feature of SAI](https://www.nxp.com/docs/en/application-note/AN12090.pdf) |Nov 2017|
|[How to use iMXRT Low Power feature](https://www.nxp.com/docs/en/application-note/AN12085.pdf) |Nov 2017|
|[Using the i.MX RT FlexRAM](https://www.nxp.com/docs/en/application-note/AN12077.pdf) |Oct 2017|
|[i.MX 6ULL Power Consumption Application Note](https://www.nxp.com/docs/en/application-note/AN5345.pdf) |Oct 2017|
|[Software ISP Application Note](https://www.nxp.com/docs/en/application-note/AN12060.pdf) |Oct 2017|
|[Building for i.MX Mature Boards](https://www.nxp.com/docs/en/application-note/AN12024.pdf) |Jul 2017|
|[i.MX 6SLL Power Consumption Measurement](https://www.nxp.com/docs/en/application-note/AN12001.pdf) |Jul 2017|
|[i.MX 6SLL Product Lifetime Usage Estimates](https://www.nxp.com/docs/en/application-note/AN11987.pdf) |Jun 2017|
|[i.MX 6SLL Migration Guide](https://www.nxp.com/docs/en/application-note/AN11988.pdf) |Jun 2017|
|[Freescale Semiconductor](https://www.nxp.com/docs/en/application-note/AN5334.pdf) |May 2017|
|[i.MX7 SABRE SD Board\xe2\x80\x94Running the Core at 1.2 GHz](https://www.nxp.com/docs/en/application-note/AN11957.pdf) |Apr 2017|
|[SCM-i.MX 6DQ PoP Memory Application Note](https://www.nxp.com/docs/en/application-note/AN11937.pdf) |Apr 2017|
|[i.MX 6 Temperature Sensor Module](https://www.nxp.com/docs/en/application-note/AN5215.pdf) |Mar 2017|
|[i.MX 6ULL Product Usage Lifetime Estimates](https://www.nxp.com/docs/en/application-note/AN5337.pdf) |Mar 2017|
|[SCM-i.MX 6SX PoP Memory Application Note](https://www.nxp.com/docs/en/application-note/AN5416.pdf) |Mar 2017|
|[i.MX 6Dual/6Quad and 6DualPlus/6QuadPlus Family Product Lifetime Usage Estimates](https://www.nxp.com/docs/en/application-note/AN4724.pdf) |Dec 2016|
|[i.MX 7DS Power Consumption Measurement](https://www.nxp.com/docs/en/application-note/AN5383.pdf) |Dec 2016|
|[Measuring Current in i.MX Applications](https://www.nxp.com/docs/en/application-note/AN5381.pdf) |Dec 2016|
|[i.MX 6ULL Migration Guide](https://www.nxp.com/docs/en/application-note/AN5350.pdf) |Oct 2016|
|[Freescale Semiconductor](https://www.nxp.com/docs/en/application-note/AN5198.pdf) |Aug 2016|
|[i.MX 6Solo/6DualLite Personality Fuses Application Note](https://www.nxp.com/docs/en/application-note/AN5324.pdf) |Aug 2016|
|[Loading Code on Cortex-M4 from Linux for the i.MX 6SoloX and i.MX 7Dual/7Solo Application Processors](https://www.nxp.com/docs/en/application-note/AN5317.pdf) |Aug 2016|
|[i.MX 6Quad/6Dual Personality Fuses Application Note](https://www.nxp.com/docs/en/application-note/AN5323.pdf) |Aug 2016|
|[MIPI\xe2\x80\x93CSI2 Peripheral on i.MX6 MPUs](https://www.nxp.com/docs/en/application-note/AN5305.pdf) |Jul 2016|
|[AN5161, Powering an i.MX 6SX-based system using the PF3000 PMIC - Application Note](https://www.nxp.com/docs/en/application-note/AN5161.pdf) |Jul 2016|
|[AN5113, Powering an i.MX 6SL based system using the PF3000 PMIC - Application Note](https://www.nxp.com/docs/en/application-note/AN5113.pdf) |Jul 2016|
|[AN5094, PF3000 layout guidelines - Application Note](https://www.nxp.com/docs/en/application-note/AN5094.pdf) |Jul 2016|
|[AN4622, MMPF0100 and MMPF0200 Layout Guidelines - Application Note](https://www.nxp.com/docs/en/application-note/AN4622.pdf) |Jul 2016|
|[i.MX 6UltraLite Power Consumption Measurement](https://www.nxp.com/docs/en/application-note/AN5170.pdf) |May 2016|
|[Interfacing mDDR and DDR2 Memories with the i.MX51](https://www.nxp.com/docs/en/application-note/AN4054.pdf) |Apr 2016|
|[i.MX 6SoloX Product Lifetime Usage Estimates - Application Notes](https://www.nxp.com/docs/en/application-note/AN5062.pdf) |Feb 2016|
|[Single Chip Module (SCM) Package-on-Package (PoP) Assembly Guide - Application Notes](https://www.nxp.com/docs/en/application-note/AN5247.pdf) |Feb 2016|
|[ARM DS-5 Development Studio Debug i.MX6UL-EVK](https://www.nxp.com/docs/en/application-note/AN5229.pdf) |Jan 2016|
|[AN4397, Common Hardware Design for i.MX 6Dual/6Quad and i.MX 6Solo/6DualLite](https://www.nxp.com/docs/en/application-note/AN4397.pdf) |Jul 2015|
|[PCI Express\xc2\xae Certification Guide for the i.MX 6SoloX](https://www.nxp.com/docs/en/application-note/AN5158.pdf) |Jul 2015|
|[How to Run the MQX\xe2\x84\xa2 RTOS on Various RAM Memories for i.MX 6SoloX - Application Note](https://www.nxp.com/docs/en/application-note/AN5127.pdf) |May 2015|
|[i.MX 6SoloX Power Consumption Measurement - AN5050](https://www.nxp.com/docs/en/application-note/AN5050.pdf) |May 2015|
|[Using the i.MX28 Power Management Unit and Battery Charger](https://www.nxp.com/docs/en/application-note/AN4199.pdf) |Apr 2015|
|[Configuring Secure JTAG for the i.MX 6 Series Family of Applications Processors](https://www.nxp.com/docs/en/application-note/AN4686.pdf) |Apr 2015|
|[Fast Image Processing with i.MX 6 Series](https://www.nxp.com/docs/en/application-note/AN4629.pdf) |Mar 2015|
|[i.MX 6 Series DDR Calibration](https://www.nxp.com/docs/en/application-note/AN4467.pdf) |Mar 2015|
|[AN4589_Rev1.fm](https://www.nxp.com/docs/en/application-note/AN4589.pdf) |Mar 2015|
|[AN5072, Introduction to Embedded Graphics with Freescale Devices-Application Notes](https://www.nxp.com/docs/en/application-note/AN5072.pdf) |Feb 2015|
|[IMX6SDL_Product_Lifetime_AN4725.fm](https://www.nxp.com/docs/en/application-note/AN4725.pdf) |Feb 2015|
|[AN5078.fm](https://www.nxp.com/docs/en/application-note/AN5078.pdf) |Feb 2015|
|[Doc_Title (same as covered Device #)](https://www.nxp.com/docs/en/application-note/AN4815.pdf) |Feb 2015|
|[AN4726_IMX6SL_Product_Lifetime.fm](https://www.nxp.com/docs/en/application-note/AN4726.pdf) |Jan 2015|
|[i.MX 6 Audio Clock Configuration Options Application Note](https://www.nxp.com/docs/en/application-note/AN4952.pdf) |Jun 2014|
|[AN4784: PCIe Certification Guide for i.MX 6Dual/6Quad and i.MX 6Solo/6DualLite - Application Note](https://www.nxp.com/docs/en/application-note/AN4784.pdf) |Oct 2013|
|[AN4715, i.MX 6Solo Power Consumption Measurement - Application Note](https://www.nxp.com/docs/en/application-note/AN4715.pdf) |Oct 2013|
|[AN4671, i.MX 6 Series HDMI Test Method for Eye Pattern and Electrical Characteristics - Application Notes](https://www.nxp.com/docs/en/application-note/AN4671.pdf) |Jul 2013|
|[i.MX53 DDR Calibration](https://www.nxp.com/docs/en/application-note/AN4466.pdf) |May 2013|
|[Secure Boot with i.MX28 HAB Version 4](https://www.nxp.com/docs/en/application-note/AN4555.pdf) |May 2013|
|[AN4604, Interfacing the MC34709 with the i.MX53 Microprocessor - Application Note](https://www.nxp.com/docs/en/application-note/AN4604.pdf) |May 2013|
|[i.MX 6 DualLite Power Consumption Measurement](https://www.nxp.com/docs/en/application-note/AN4576.pdf) |Mar 2013|
|[AN4603, Power Management Design Guidelines for the i.MX50x Family of Microprocessors - Application Note](https://www.nxp.com/docs/en/application-note/AN4603.pdf) |Jan 2013|
|[AN4620, Interfacing the MC34708 with an External Battery Charger - Application Note](https://www.nxp.com/docs/en/application-note/AN4620.pdf) |Dec 2012|
|[i.MX 6 Series Thermal Management Guidelines](https://www.nxp.com/docs/en/application-note/AN4579.pdf) |Dec 2012|
|[AN4580_Rev0_wrkng.fm](https://www.nxp.com/docs/en/application-note/AN4580.pdf) |Dec 2012|
|[AN4641.fm](https://www.nxp.com/docs/en/application-note/AN4641.pdf) |Dec 2012|
|[Interfacing the MC34709 with an External Battery Charger, AN4619 - Application Note](https://www.nxp.com/docs/en/application-note/AN4619.pdf) |Nov 2012|
|[i.MX 6Dual/6Quad Power Consumption Measurement](https://www.nxp.com/docs/en/application-note/AN4509.pdf) |Oct 2012|
|[AN4547_wrkng.fm](https://www.nxp.com/docs/en/application-note/AN4547.pdf) |Oct 2012|
|[Using Open Source Debugging Tools for Linux on i.MX Processors](https://www.nxp.com/docs/en/application-note/AN4553.pdf) |Jul 2012|
|[i.MX28 Ethernet Performance on Linux](https://www.nxp.com/docs/en/application-note/AN4544.pdf) |Jun 2012|
|[Changing the i.MX31 USB PHY](https://www.nxp.com/docs/en/application-note/AN4130.pdf) |May 2012|
|[AN4270_Rev1.fm](https://www.nxp.com/docs/en/application-note/AN4270.pdf) |Feb 2012|
|[AN4380.fm](https://www.nxp.com/docs/en/application-note/AN4380.pdf) |Oct 2011|
|[AN4227, MC13892 Charger Operation](https://www.nxp.com/docs/en/application-note/AN4227.pdf) |Aug 2011|
|[AN4271.fm](https://www.nxp.com/docs/en/application-note/AN4271.pdf) |Feb 2011|
|[test.fm](https://www.nxp.com/docs/en/application-note/AN4274.pdf) |Feb 2011|
|[Modifying Bootloader and Kernel to Support a Different SDRAM on the i.MX51 Using WinCE 6.0\xe2\x84\xa2](https://www.nxp.com/docs/en/application-note/AN4202.pdf) |Feb 2011|
|[i.MX51 Power-Up Sequence](https://www.nxp.com/docs/en/application-note/AN4053.pdf) |Oct 2010|
|[imx28_layout_guidelines_rev_0.fm](https://www.nxp.com/docs/en/application-note/AN4215.pdf) |Sep 2010|
|[Modifying Bootloader and Kernel to Support a Different SDRAM on the i.MX25 using WinCE 6.0\xe2\x84\xa2i.MX25 using WinCE 6.0](https://www.nxp.com/docs/en/application-note/AN4200.pdf) |Sep 2010|
|[Architectural Differences between the i.MX23, i.MX25, and i.MX28](https://www.nxp.com/docs/en/application-note/AN4198.pdf) |Sep 2010|
|[System-80 Asynchronous Display on the i.MX31 WINCE 6.0 PDK](https://www.nxp.com/docs/en/application-note/AN4180.pdf) |Aug 2010|
|[Different Display Configurations on the i.MX31 Linux PDK](https://www.nxp.com/docs/en/application-note/AN4182.pdf) |Aug 2010|
|[U-Boot for i.MX25 Based Designs](https://www.nxp.com/docs/en/application-note/AN4171.pdf) |Jul 2010|
|[U-Boot for i.MX35 based Designs](https://www.nxp.com/docs/en/application-note/AN4172.pdf) |Jul 2010|
|[Media Streaming to i.MX31 PDK through Wireless LAN](https://www.nxp.com/docs/en/application-note/AN4174.pdf) |Jul 2010|
|[U-Boot for i.MX51 Based Designs](https://www.nxp.com/docs/en/application-note/AN4173.pdf) |Jul 2010|
|[Hardware Configurations for the i.MX Family USB Modules](https://www.nxp.com/docs/en/application-note/AN4136.pdf) |Jun 2010|
|[Multi-NAND Disks Implementation Guide](https://www.nxp.com/docs/en/application-note/AN4139.pdf) |Jun 2010|
|[BINFS Implementation Guide](https://www.nxp.com/docs/en/application-note/AN4137.pdf) |Jun 2010|
|[i.MX51 WinCE Clock Setting](https://www.nxp.com/docs/en/application-note/AN4140.pdf) |Jun 2010|
|[Program NAND with ROM Programmer Implementation Guide](https://www.nxp.com/docs/en/application-note/AN4138.pdf) |Jun 2010|
|[i.MX51 EVK Supply Current Measurements](https://www.nxp.com/docs/en/application-note/AN4051.pdf) |May 2010|
|[Getting Started with OpenGL ES, PVR Libraries, and Tools for the i.MX31 PDK](https://www.nxp.com/docs/en/application-note/AN4131.pdf) |May 2010|
|[3D Math Overview and 3D Graphics Foundations](https://www.nxp.com/docs/en/application-note/AN4132.pdf) |May 2010|
|[How to Run BSP Unit Test on Windows CE Using i.MX Platforms](https://www.nxp.com/docs/en/application-note/AN4133.pdf) |May 2010|
|[Changing the i.MX25 NAND Flash Model for Windows Embedded CE 6.0](https://www.nxp.com/docs/en/application-note/AN4111.pdf) |May 2010|
|[i.MX31 PDK Power Measurement with GUI](https://www.nxp.com/docs/en/application-note/AN4061.pdf) |Feb 2010|
|[i.MX233 CPU and HCLK Power Saving Features](https://www.nxp.com/docs/en/application-note/AN4050.pdf) |Jan 2010|
