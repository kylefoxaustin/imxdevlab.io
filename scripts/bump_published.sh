#/bin/sh

datetime=`date "+%Y-%m-%d %H:%M:%S"`

if [ -z "$1" ]
  then
    echo "Add the parameter 'last_modified_at: $datetime' above the date line in a post.
It uses the system date and time.
   Usage:
     $0 <path to the post to be bumped>

"
  exit
fi

sed -i "/date:/ a last_modified_at: $datetime" $1

echo "Please see the change with
git diff $1"
