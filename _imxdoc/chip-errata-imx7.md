---
title: "i.MX7 SoC Errata"
excerpt: "A page to list all the links to the online Errata for the imx7 chips"
---
{% include imxdoc/rm-introduction.md %}
{% include imxdoc/ce-imx7-table.md %}

* The file `IMX7D_2N09P.pdf` applies to iMX7D and iMX7S.
