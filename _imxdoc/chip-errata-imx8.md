---
title: "i.MX8 SoC Errata"
excerpt: "A page to list all the links to the online Errata for the imx8 chips"
---
{% include imxdoc/rm-introduction.md %}
{% include imxdoc/ce-imx8-table.md %}
