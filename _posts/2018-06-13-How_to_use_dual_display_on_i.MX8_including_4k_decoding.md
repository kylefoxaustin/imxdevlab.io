---
title: How to use dual display on i.MX8 including 4k decoding
date: 2018-06-13 13:34:49
author: marco
categories: [ Tutorial ]
tags: [ 4k, dual_display, L4.9.51_1.0.0_GA, GStreamer, H.264, H.265, IMX8M, IMX8MQ, imx8mqevk, IMX-MIPI-HDMI ]
---
In this first post about ```i.MX 8M``` SoCs, you can check how easy is to use one or two HDMI displays and decoding 4k video file at 60fps in the [i.MX 8MQ EVK](https://www.nxp.com/products/processors-and-microcontrollers/applications-processors/i.mx-applications-processors/i.mx-8-processors/i.mx-8m-family-armcortex-a53-cortex-m4-audio-voice-video:i.MX8M).

# 4k video playback
The i.MX8MQ has the capability to decoder H.264 and H.265 4k videos at 60 FPS with HDR support.
For this, let's start [downloading](https://www.nxp.com/webapp/Download?colCode=L4.9.51_IMX8MQ_GA&appType=license&location=null&Parent_nodeId=1337699481071706174845&Parent_pageType=product) and deploying the ```Linux 4.9.51_1.0.0_GA``` image from the NXP web site to the SDCard:
```
$ sudo dd if=fsl-image-validation-imx-xwayland-imx8mqevk.sdcard of=/dev/sbX
```

Download a 4k video sample for the test. I would like to suggest the [4kmedia.org](http://4kmedia.org) website and copy it to the SDCard:

```
$ sudo cp Download/4k_video_sample /media/USER/ROOTFS_PATH/home/root; sync
```

Depending of the 4k video size, it may be necessary to change the SDCard free space in order to copy the file to the SDCard. I recommend to use the GParted for it.
{: .notice--warning}

The HDMI output is configured to support the 3820x2160 size by default. So, boot the board and enter with the GStreamer pipeline below. Do not forget to use the ```force-hantrotile=true``` property to enable the video framebuffer compression (DTRC):
### For H.265 with HDR container on .mp4 format
```
gst-launch-1.0 filesrc location=4k_video_sample.mp4 ! video/quicktime ! qtdemux ! queue ! vpudec ! queue ! kmssink force-hantrotile=true
```
<iframe width="560" height="315" src="https://www.youtube.com/embed/XOMrtr1ripI" frameborder="0" allowfullscreen></iframe>

# Dual display
The i.MX 8MQ has also the capability to use up to two HDMI displays output(4k@60 + 720p@60). In order to accomplish that, the board uses the ```MIPI-DSI``` connector with the [IMX-MIPI-HDMI](https://www.nxp.com/part/IMX-MIPI-HDMI) adapter:

<center><img src="{{ base_path }}/images/IMX-MIPI-HDMI.png" alt="IMX-MIPI-HDMI adapter" align="middle" width="800"/></center>

Using the same image from the last section, just download a HD video and copy it to the board too. I could found a 720p@60fps sample video in the [kodi wiki](https://kodi.wiki/view/Samples):
```
$ sudo cp Download/720p_video_sample /media/USER/ROOTFS_PATH/home/root
```

The last point you need to use the dual display mode is by changing the ```.dtb``` file. So start the board and change the U-Boot ```fdt_file``` variable:
```console
=> setenv fdt_file fsl-imx8mq-evk-dual-display.dtb
=> saveenv
```

Reset the board and enter with the GStreamer pipelines below:
### For H.265 container on .mp4 format
```
gst-launch-1.0 filesrc location=4k_video_sample.mp4 typefind=true ! video/quicktime ! qtdemux ! queue max-size-time=0 ! vpudec ! queue max-size-time=0 ! kmssink force-hantrope=true sync=false &
```

### For H.264 container on .ts format
```
gst-launch-1.0 filesrc location=720p_video_sample.ts typefind=true ! video/mpegts ! tsdemux ! queue max-size-time=0 ! vpudec ! queue max-size-time=0 ! waylandsink
```
<iframe width="560" height="315" src="https://www.youtube.com/embed/ZvUFS4l9txQ" frameborder="0" allowfullscreen></iframe>
