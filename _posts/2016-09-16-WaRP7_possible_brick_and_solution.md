---
title: WaRP7 possible brick and solution
redirect_from: blog/tutorial/2016/09/16/WaRP7_possible_brick_and_solution/
date: 2016-09-16 11:02:41
author: andres
categories: [Tutorial] 
tags: [warp7, MMC, uboot] 
---
It can be the case that the user partition on the WaRP7 is unlocked. Burning an image that doesn't contain U-Boot into the eMMC while on this state erases the original U-Boot. To avoid this, the user can run the commands below from the U-Boot prompt:

```console
=> mmc partconf 0 1 1 0
=> saveenv
```

This command makes the user partition read-only and prevents u-boot from being changed.

If you delete U-Boot by mistake and your board does not boot, please load U-boot from your host machine using imx_usb_loader. Run the command above once you get to the U-boot prompt to prevent U-boot from being deleted again. You can follow the steps in this link <https://github.com/WaRP7/WaRP7-User-Guide/blob/master/04-Chapter/U-boot.adoc#obtaining-an-u-boot-image> to learn how to boot U-Boot from your host machine.

Inversely if your partition is locked and you would like to update U-Boot, follow the procedure as described on this link <https://github.com/WaRP7/WaRP7-User-Guide/blob/master/04-Chapter/U-boot.adoc#updating-an-u-boot-image>, you can unlock the user partition using the commands:

```console
=> mmc partconf 0 1 0 0
=> saveenv
```

**Update:** The original post used the term "raw partition" instead of "user partition". It was pointed out that the term "raw partition" was not completely accurate, reason for which it was changed.
