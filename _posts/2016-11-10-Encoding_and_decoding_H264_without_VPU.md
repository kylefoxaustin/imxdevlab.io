---
title: Encoding and decoding H.264 without VPU
date: 2016-11-10 16:03:28
author: marco
categories: [ Tutorial ]
tags: [ yocto, GStreamer, H264, encoder, decoder, IMX6UL, IMX7D ]
---
There are SoCs such as i.MX6UL and i.MX7D that do not have VPU to accelerate video encoding or decoding. However, there are GStreamer plugins capable of encoding and decoding videos by software that can be used on SoCs that do not have VPU.
The following sections show all the necessary steps to get H.264 plugins installed on imx6ulevk and imx7dsabresd Yocto Project image.

### Packages and Licenses

Add in your `<build-dir>/conf/local.conf` file:
```
CORE_IMAGE_EXTRA_INSTALL += "gstreamer1.0-plugins-ugly-meta packagegroup-fsl-gstreamer1.0-commercial gst-ffmpeg"
LICENSE_FLAGS_WHITELIST += "commercial"
CORE_IMAGE_EXTRA_INSTALL += "gstreamer1.0-plugins-ugly"
```

### Recipes

Open your favorite text editor and copy the text below into a new text file:
```
PACKAGECONFIG_mx7d = "x264"
PACKAGECONFIG_mx6ul = "x264"
```
Save the file as `gstreamer1.0-plugins-ugly_%.bbappend` and put it in `<build-dir>/sources/meta-fsl-arm/recipes-multimedia/gstreamer/`.

Now, just run a bitbake to get the packages installed on the desired image.
